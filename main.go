package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

var (
	re = regexp.MustCompile(`id="([a-zA-Z0-9]*)"`)
)

func main() {
	for page := 100; page < 3000; page++ {
		url := "https://imgur.com/search/score"
		if page > 0 {
			url += "/page/" + strconv.Itoa(page)
		}
		url += "?q=ext%3Agif"
		resp, err := http.Get(url)
		if err != nil {
			panic(err)
		}
		ids, err := readResp(resp)
		if err != nil {
			panic(err)
		}
		for _, id := range ids {
			fmt.Println("https://i.imgur.com/" + id + ".gif")
		}
	}
}

func readResp(resp *http.Response) ([]string, error) {
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(body), "\n")
	var ids []string
	for _, line := range lines {
		if !strings.Contains(line, `class="post"`) {
			continue
		}
		matches := re.FindStringSubmatch(line)
		if len(matches) < 2 {
			continue
		}
		ids = append(ids, matches[1])
	}
	return ids, nil
}
